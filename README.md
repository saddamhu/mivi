# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Mivi Test
* 0.0.0

### How do I get set up? ###

* bundle install
* open irb 
* $: << 'lib'        #Add The lib directory to the list of directories that ruby uses to load classes or modules.
* require 'mivi'
* Mivi.print_data "PATH_TO_FILE/collection.json"


### How do I run unit testing? ###
* ruby test/unit/mivi_test.rb
