Gem::Specification.new do |s|
  s.name        = 'mivi'
  s.version     = '0.0.0'
  s.date        = '2018-03-16'
  s.summary     = "mivi!"
  s.description = "Mivi Test"
  s.authors     = ["saddam husain"]
  s.email       = 'saddam20.hussain@gmail.com'
  s.files       = ["lib/mivi.rb"]
  s.homepage    =
    'http://rubygems.org/gems/mivi'
  s.license       = 'MIT'
end