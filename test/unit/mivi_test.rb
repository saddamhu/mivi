require 'minitest/autorun'
require 'json'

class MiviTest < Minitest::Test

  def test_mivi
    user_details,file_data,phone_number,email,name,product_name = read_json

    assert user_details.has_key?("contact-number")
    assert user_details.has_key?("email-address")
    assert user_details.has_key?("title")
    assert user_details.has_key?("first-name")
    assert user_details.has_key?("last-name")
    assert file_data["included"][2]["attributes"].has_key?("name")

    phone_number_copy,email_copy,name_copy,product_name_copy=set_copy_data
    assert_equal phone_number, phone_number_copy
    assert_equal email, email_copy
    assert_equal name, name_copy
    assert_equal product_name, product_name_copy
  end

  def read_json
    file = '././lib/collection.json'
    file_data = File.open(file, "r+").read
    file_data = JSON file_data
    user_details = file_data["data"]["attributes"]
    phone_number = user_details["contact-number"]
    email = user_details["email-address"]
    name = user_details["title"]+" "+user_details["first-name"]+" "+user_details["last-name"]
    product_name = file_data["included"][2]["attributes"]["name"]
    return user_details,file_data,phone_number,email,name,product_name
  end

  def set_copy_data
    phone_number_copy = "0404000000"
    email_copy = "test@mivi.com"
    name_copy = "Ms Joe Bloggs"
    product_name_copy = "UNLIMITED 7GB"
    return phone_number_copy,email_copy,name_copy,product_name_copy
  end

  
end
