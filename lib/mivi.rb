require 'json'
class Mivi
  def self.print_data(file)
  	begin
	    file_data = File.open(file, "r+").read
	    file_data = JSON file_data
	    user_details = file_data["data"]["attributes"]
	    phone_number = user_details["contact-number"]
	    email = user_details["email-address"]
	    name = user_details["title"]+" "+user_details["first-name"]+" "+user_details["last-name"]
	    product_name = file_data["included"][2]["attributes"]["name"]
	    puts "Phone Number: #{phone_number}"
	    puts "Email: #{email}"
	    puts "Name: #{name}"
	    puts "Product Name: #{product_name}"
	  rescue => e 
	  	p e.message
	  end
  end
end